﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterListManager : MonoBehaviour
{
    CharacterListManager instance;
    public CharacterList characterScript;
    public GameObject objectCharacterActual;
    public string characterActualNombre;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else 
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (characterScript != null)
        { //Si existe este script
            if (characterScript.lista[characterScript.index] != null) //Si hay objetos en el script en esa posicion del index.
            {
                objectCharacterActual = characterScript.lista[characterScript.index];
                characterActualNombre = objectCharacterActual.name;
            }
        }
    }
}
