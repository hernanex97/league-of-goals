﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterList : MonoBehaviour
{
    public GameObject[] lista;
    public GameObject characterActual;    
    public int index;


    private void Start()
    {
        lista = new GameObject[transform.childCount];

        //Llena el array con los characters
        for (int i = 0; i < transform.childCount; i++)
        {
            lista[i] = transform.GetChild(i).gameObject;
        }

        //Le decimos que apague el render
        foreach (GameObject go in lista)
        {
            go.SetActive(false);
        }

        if (lista[0])
        {
            lista[0].SetActive(true);
        }
            
    }

    private void Update()
    {
        characterActual = lista[index];       
    }


    public void Izquierda()
    {
        //Apagar el modelo actual
        lista[index].SetActive(false);

        index--; //index -=; idnex = index -1;
        if(index < 0)
        {
            index = lista.Length - 1;
        }

        //Prender el modelo actual
        lista[index].SetActive(true);
    }

    public void Derecha()
    {
        //Apagar el modelo actual
        lista[index].SetActive(false);

        index++; //index -=; index = index -1;
        if (index == lista.Length)
        {
            index = 0;
        }

        //Prender el modelo actual
        lista[index].SetActive(true);
    }

    

}
