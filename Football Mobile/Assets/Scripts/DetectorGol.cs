﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class DetectorGol : MonoBehaviourPunCallbacks
{
    public GameManager gameManager;
    public Transform transformP;
    GameTimerScipt GameTimerScipt;
    public AudioClip ClipGol;

    public void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        GameTimerScipt = FindObjectOfType<GameTimerScipt>();
        saqueInicial();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pelota")
        {
            //GOL EQUIPO AZUL
            if (gameObject.tag == "ArcoRojo")
            {
                gameManager.seAnotaGol = true;
                gameManager.golAzul = true;
                gameManager.golesEquipoAzul++;
                gameManager.photonView.RPC("ActualizarGolAzul", RpcTarget.AllViaServer, gameManager.golesEquipoAzul);
                gameManager.saqueEquipoRojo();
            }

            //GOL EQUIPO ROJO
            if (gameObject.tag == "ArcoAzul")
            {
                gameManager.seAnotaGol = true;
                gameManager.golRojo = true;
                gameManager.golesEquipoRojo++;
                gameManager.photonView.RPC("ActualizarGolRojo", RpcTarget.AllViaServer, gameManager.golesEquipoRojo);
                gameManager.saqueEquipoAzul();
            }

            AudioManager.Instance.playSonido(ClipGol, 5f);
            GameTimerScipt.tiempoCorre = false;
            other.gameObject.transform.position = transformP.position;
            other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            other.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
            
       
    }

    void saqueInicial()
    {
        if (gameManager.pared1 != null)
            gameManager.pared1.SetActive(false);

        if (gameManager.pared2 != null)
            gameManager.pared2.SetActive(false);

        if (gameManager.semiCRed != null)
            gameManager.semiCRed.SetActive(false);

        if (gameManager.semiCBlue != null)
            gameManager.semiCBlue.SetActive(false);
    }
        
    


    


}
