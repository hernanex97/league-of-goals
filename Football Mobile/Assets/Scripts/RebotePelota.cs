﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RebotePelota : MonoBehaviour
{

    Rigidbody rb;
    public float fuerzaRebote;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision other)
    {
        //print(other.collider.tag);
        if (other.collider.tag == "ParedPelota") { 

            Vector3 direccion =other.contacts[0].normal;
            //print(other.contacts[0].normal);
            rb.AddForce(direccion * fuerzaRebote);

        }
    }
}
