﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    Collider colliderExterior;
    MeshRenderer meshRenderer;
    public KeyCode teclaDisparo;
    public Renderer rend;
    ImpactoPelota pelotaScript;

  
    void Start()
    {
        pelotaScript = GetComponent<ImpactoPelota>();
        rend = GetComponent<Renderer>();
        colliderExterior = GetComponent<Collider>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if (Input.GetKey(teclaDisparo))
        {
            colliderExterior.enabled = true;
            meshRenderer.enabled = true;
            rend.material.color = Color.white;                  
        }
        else
        {
            colliderExterior.enabled = false;
            meshRenderer.enabled = false;
        }
    }



}
