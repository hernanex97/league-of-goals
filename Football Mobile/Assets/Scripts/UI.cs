﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;


public class UI : MonoBehaviourPunCallbacks
{
    public Text scoreRojo;
    public Text scoreAzul;
    public Text calentamientoText;
    public Text pingText;
    public float tiempo;
    public Text timerText;
    public bool enJuego = true;
    PhotonPlayer phPlayer;
    public PhotonView phView;
    public GameObject ganadorAzulText, ganadorRojoText, empateText, golAzulText, golRojoText;

    public void Start()
    {
        phPlayer = gameObject.GetComponent<PhotonPlayer>();
        enJuego = true;
        tiempo = 0;
        StartCoroutine(Reloj());

        

    }

    public void Update()
    {
        
        
        if(timerText !=null)
        timerText.text = tiempo.ToString();

        int ping = PhotonNetwork.GetPing();
        pingText.text = ping.ToString() + "ms";

    }



    IEnumerator Reloj()
    {
        while (enJuego) 
        {
            yield return new WaitForSeconds(1f);
            tiempo++;
        }
    }
}



