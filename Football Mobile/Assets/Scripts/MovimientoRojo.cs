﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoRojo : MonoBehaviour
{
    public float fuerzaEmpuje;
    Rigidbody rb;
    GameTimerScipt GameTimerScipt;
    AudioClip[] clipsPatada;
    

    void Start()
    {
        GameTimerScipt = FindObjectOfType<GameTimerScipt>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 moverseHorizontal = new Vector3(0, 0, 1);
        Vector3 moverseVertical = new Vector3(1, 0, 0);

      // if (Input.GetKey(KeyCode.UpArrow))
      // {        
      //     rb.AddForce(moverseHorizontal * fuerzaEmpuje);
      // }
      // if (Input.GetKey(KeyCode.DownArrow))
      // {           
      //     rb.AddForce(-moverseHorizontal * fuerzaEmpuje);
      // }
      // if (Input.GetKey(KeyCode.LeftArrow))
      // {          
      //     rb.AddForce(-moverseVertical * fuerzaEmpuje);
      // }
      // if (Input.GetKey(KeyCode.RightArrow))
      // {            
      //     rb.AddForce(moverseVertical * fuerzaEmpuje);
      // }   

        

    }

    void OnTriggerEnter(Collider other)
    {

        if(other.gameObject.tag == "Pelota")
        {
            GameTimerScipt.tiempoCorre = true;
        }    
    }

    public void ElegirSonidoChoque()
    {
        int sonidoNumero = Random.Range(0, clipsPatada.Length);
        AudioManager.Instance.playSonido(clipsPatada[sonidoNumero], 0.1f);
    }

}
