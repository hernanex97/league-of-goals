﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;


public class ImpactoPelota : MonoBehaviourPun
{
    public float fuerzaImpacto;
    public bool fueTocada;
    public GameManager gameManager;
    public KeyCode teclaDisparo;
    Rigidbody rb;
    GameTimerScipt GameTimerScipt;
    DetectorGol detectorGolScript;
    public bool pateaTouch;
    public AudioClip[] clipsPelota;
    public Vector3 direccion;

    private void Start()
    {
        pateaTouch = false;
        fueTocada = false;
        rb = GetComponent<Rigidbody>();
        GameTimerScipt = FindObjectOfType<GameTimerScipt>();
        gameManager = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        if (fueTocada)
        {
            gameManager.semiCBlue.SetActive(false);
            gameManager.semiCRed.SetActive(false);
            gameManager.pared1.SetActive(false);
            gameManager.pared2.SetActive(false);
        }
    }


    private void OnCollisionStay(Collision collision)
    {
        //Lógica de Saque.
        if (collision.gameObject.tag == "PlayerRojo" || collision.gameObject.tag == "PlayerAzul")
        {
            GameTimerScipt.tiempoCorre = true;
            fueTocada = true;
        }

        //Lógica de Disparo/Patada.
        if (collision.gameObject.tag == "PlayerRojo" && pateaTouch || collision.gameObject.tag == "PlayerAzul" && pateaTouch)
        {
            direccion = collision.contacts[0].normal;
            print(direccion);
            rb.AddForce(direccion * fuerzaImpacto);
            ElegirSonidoChoque();
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        pateaTouch = false;
    }

    public void ElegirSonidoChoque()
    {
        int sonidoNumero = Random.Range(0, clipsPelota.Length);
        AudioManager.Instance.playSonido(clipsPelota[sonidoNumero], 5f);
    }

    public void Patear()
    {
        pateaTouch = true;
    }

    private void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)
        {
            stream.SendNext(rb.position);
            stream.SendNext(rb.rotation);
            stream.SendNext(rb.velocity);
        }
        else
        {
            rb.position = (Vector3)stream.ReceiveNext();
            rb.rotation = (Quaternion)stream.ReceiveNext();
            rb.position = (Vector3)stream.ReceiveNext();
             
            float lag = Mathf.Abs((float) (PhotonNetwork.Time - info.SentServerTimestamp));
            rb.position += rb.velocity * lag; 
             
        }
    }





}
