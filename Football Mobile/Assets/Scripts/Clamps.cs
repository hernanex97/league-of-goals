﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clamps : MonoBehaviour
{
    
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -86f, 85f), 
                                            Mathf.Clamp(transform.position.y, 1.5f, 13f), 
                                                Mathf.Clamp(transform.position.z, -37f, 37f));  //para que no se caiga por los laterales
    }
}
