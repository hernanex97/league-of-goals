﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSwitcher : MonoBehaviour
{

     public Camera[] cameras;
     public int currentCameraIndex = 0;
     public Camera currentCamera;
    
   
    
    public void Start()
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].enabled = false;
        }

        cameras[currentCameraIndex].enabled = true;
        currentCamera = cameras[currentCameraIndex];

    }

    public void Cambiocamara()
    {
        if (currentCameraIndex < cameras.Length -1)
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex++;
            cameras[currentCameraIndex].enabled = true;
            currentCamera = cameras[currentCameraIndex];
            // ChangeView();

        }
        else
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex = 0;
            cameras[currentCameraIndex].enabled = true;
            currentCamera = cameras[currentCameraIndex];

            // ChangeView();
        }
    }

    /*public void ChangeView()
        {

        cameras[currentCameraIndex].enabled = false;
        //currentCameraIndex++;
        cameras[currentCameraIndex].enabled = true;
            
        }
        */
}
