﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class ControlesManager : MonoBehaviourPun
{
    public float fuerzaEmpuje;
    GameTimerScipt GameTimerScipt;
    AudioClip[] clipsPatada;
    CameraWork _cameraWork;
    
    protected Joystick joystick;
    protected Joybutton joybutton;
    public int acelerador = 1000;   

    void Start()
    {
        
        joystick=FindObjectOfType<Joystick>();
        joybutton = FindObjectOfType<Joybutton>();

        CameraWork _cameraWork = this.gameObject.GetComponent<CameraWork>();

        if (_cameraWork != null)
        {
            if (photonView.IsMine)
            {
                _cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> CameraWork Component on playerPrefab.", this);
        }

        GameTimerScipt = FindObjectOfType<GameTimerScipt>();
    }

    
    void FixedUpdate()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
        {
            return;
        }

        if(photonView.IsMine == true && PhotonNetwork.IsConnected == true)
        {

            Rigidbody rigidbody = GetComponent<Rigidbody>();         
            rigidbody.velocity = new Vector3(joystick.Horizontal+ Input.GetAxis("Horizontal"), rigidbody.velocity.y, joystick.Vertical + Input.GetAxis("Horizontal")) * acelerador;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Pelota")
        {
            GameTimerScipt.tiempoCorre = true;
        }
    }

    //public void ElegirSonidoChoque()
    //{
    //    int sonidoNumero = Random.Range(0, clipsPatada.Length);
    //    AudioManager.Instance.playSonido(clipsPatada[sonidoNumero], 0.1f);
    //}
}

