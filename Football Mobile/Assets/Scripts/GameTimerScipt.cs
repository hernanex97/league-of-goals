﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimerScipt : MonoBehaviour
{
    public Text gameTimerText;
    public float gameTimer = 0f;

    ImpactoPelota pelotaScript;

    public bool tiempoCorre;

    private void Start()
    {
        pelotaScript = FindObjectOfType<ImpactoPelota>();
        tiempoCorre = false;
    }


    void Update()
    {
        if(tiempoCorre == true)
        {
            gameTimer += Time.deltaTime;
        }
        

        int segundos = (int)(gameTimer % 60);
        int minutos = (int)(gameTimer / 60);

        string timerString = string.Format("{0:00}:{1:00}", minutos, segundos);

        gameTimerText.text = timerString;

    }
}
