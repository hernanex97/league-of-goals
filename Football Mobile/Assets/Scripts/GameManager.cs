﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;



public class GameManager : MonoBehaviourPun//, IPunObservable
{
    public GameInstantiates game;
    PhotonView phView;
    public int golesEquipoRojo;
    public int golesEquipoAzul;
    public bool seAnotaGol;
    UI ui;
    public static GameObject playersRojos;
    public static GameObject playersAzules;
    GameObject playerRojoPosicionInicial;
    GameObject playerAzulPosicionInicial;
    ImpactoPelota pelotaScript;
    public DetectorGol detectorGolScript;
    public GameObject detectorGolRojo;
    public GameObject detectorGolAzul;
    public float limiteTiempo;
    public int golesParaGanar;
    public bool golAzul;
    public bool golRojo;
    public bool ganoAzul;
    public bool ganoRojo;
    public bool empiezaElJuego;

    public GameObject pared1, pared2, semiCBlue, semiCRed;

    void Awake()
    {   
        seAnotaGol = false;
        golRojo = golAzul = false;

        ////Destruye el GameManager si no es controlado por ÉSTE photonView
        //if (!photonView.IsMine && GetComponent<GameManager>() != null)
        //    Destroy(GetComponent<GameManager>());
    }

    public void Start()
    {
        pelotaScript = FindObjectOfType<ImpactoPelota>();
        detectorGolScript = FindObjectOfType<DetectorGol>();
        ui = FindObjectOfType<UI>();
        golesEquipoAzul = 0;
        golesEquipoRojo = 0;
        empiezaElJuego = true;
        resetearTextos();
        golesParaGanar = 3;
        limiteTiempo = 3f;
        ui.calentamientoText.text = "CALENTAMIENTO";
        detectorGolRojo.SetActive(false);
        detectorGolAzul.SetActive(false);
    }

    private void Update()
    {
        //if (!phView.IsMine) return;
        print(golesEquipoAzul + "Gol EQUIPO AZUUUUUUL");
        print(golesEquipoRojo + "Gol EQUIPO ROJOOOOO");
        //Aca se hace la logica cuando convierte un gol
        if (seAnotaGol)
        {
            mostrarCarteles();//Mostrar carteles  
        }
        print(PhotonNetwork.CurrentRoom.PlayerCount + " PlayerCount");
        print(PhotonNetwork.CurrentRoom.MaxPlayers + " MaxPlayers");

        //Calentamiento
        if ((PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers && empiezaElJuego) || Input.GetKey(KeyCode.O)) //si el conteo de players en igual a la variable de players maximas del room
        {
            //Reseteo de pos y se desactiva una de las dos paredes 
            reseteoPosiciones();
            detectorGolRojo.SetActive(true);
            detectorGolAzul.SetActive(true);
            ui.calentamientoText.text = "";
            semiCBlue.SetActive(false);
            empiezaElJuego = false;
        }
    }

    void reseteoPosiciones()
    {
        game = FindObjectOfType<GameInstantiates>();

        playersRojos = GameObject.FindGameObjectWithTag("PlayerRojo");
        playersAzules = GameObject.FindGameObjectWithTag("PlayerAzul");

        if (playersRojos)
        {
            int posNueva = Random.Range(0, GameInstantiates.gInstantiates.spawnsRed.Length);
            playersRojos.gameObject.transform.position = game.spawnsRed[posNueva].transform.position;
        }

        if (playersAzules)
        {
            int posNueva = Random.Range(0, GameInstantiates.gInstantiates.spawnsBlue.Length);
            playersAzules.gameObject.transform.position = game.spawnsBlue[posNueva].transform.position;
        }

        seAnotaGol = false;
    }


    public void saqueEquipoRojo()
    {
        pared1.SetActive(true);
        pared2.SetActive(true);
        semiCRed.SetActive(false);
        semiCBlue.SetActive(true);
        pelotaScript.fueTocada = false;
    }


    public void saqueEquipoAzul()
    {
        pared1.SetActive(true);
        pared2.SetActive(true);
        semiCRed.SetActive(true);
        semiCBlue.SetActive(false);
        pelotaScript.fueTocada = false;
    }

    public void ParedesPrendidas()
    {
        pared1.SetActive(true);
        pared2.SetActive(true);
        semiCRed.SetActive(true);
        semiCBlue.SetActive(true);
    }

    void mostrarCarteles()
    {
        //Carteles goles
        if (golAzul)
        {
            ui.scoreAzul.text = golesEquipoAzul.ToString();
            mostrarCartelGolAzul();
            golAzul = false;
        }
        else if (golRojo) 
        {
            ui.scoreRojo.text = golesEquipoRojo.ToString();
            mostrarCartelGolRojo();
            golRojo = false;
        }
    }

    void cartelGanoAzul()
    {        
        ui.ganadorAzulText.SetActive(true);
        ganoAzul = false;
    }
   
    void cartelGanoRojo()
    {
        ui.ganadorRojoText.SetActive(true);
        ganoRojo = false;
    }

    void mostrarCartelGolRojo()
    {
        //MUESTRO CARTEL GOL ROJO
        ui.golRojoText.SetActive(true);
        StartCoroutine("DelayCartelesUI");

        reseteoPosiciones();
        //CHEQUEO SI EL EQUIPO ROJO ALCANZO LA CANT DE GOLES PARA GANAR
        if (golesParaGanar == golesEquipoRojo)
        {
            //MOSTRAR CARTEL GANADOR EQUIPO ROJO
            ganoRojo = true;
        }

    }

    void mostrarCartelGolAzul()
    {
        //MUESTRO CARTEL GOL AZUL
        ui.golAzulText.SetActive(true);
        StartCoroutine("DelayCartelesUI");
        //CHEQUEO SI EL EQUIPO AZUL ALCANZO LA CANT DE GOLES PARA GANAR
        if (golesParaGanar == golesEquipoAzul)
        {
            ganoAzul = true;
            //MOSTRAR CARTEL GANADOR EQUIPO AZUL
        }
    }

    void cartelEmpate()
    {
        ui.empateText.SetActive(true);
    }

    void resetearTextos()
    {
        ui.ganadorAzulText.SetActive(false);
        ui.ganadorRojoText.SetActive(false);
        ui.empateText.SetActive(false);
        ui.golAzulText.SetActive(false);
        ui.golRojoText.SetActive(false);
    }
       

    IEnumerator DelayCartelesUI()//corutina gol azul esta corutina es para el gol
    {
        //mostrar cartel gol azul
        yield return new WaitForSeconds(1f);
        resetearTextos();//limpia los textos
        if (ganoAzul) {
            yield return new WaitForSeconds(1f);
            cartelGanoAzul();
        }
        if (ganoRojo) {
            yield return new WaitForSeconds(1f);
            cartelGanoRojo();
        }
        reseteoPosiciones();
    }

    IEnumerator PosicionesWait()
    {
        yield return new WaitForSeconds(3f);
        
    }

    //void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.IsWriting)
    //    {
    //        stream.SendNext(ui.timerText.text);
    //        stream.SendNext(golesEquipoRojo);
    //        stream.SendNext(golesEquipoAzul);
    //        stream.SendNext(pelotaScript.fueTocada);
    //    }
    //
    //    else if (stream.IsReading)
    //    {
    //        ui.timerText.text = (string)stream.ReceiveNext();
    //        golesEquipoAzul = (int)stream.ReceiveNext();
    //        golesEquipoRojo = (int)stream.ReceiveNext();
    //        pelotaScript.fueTocada = (bool)stream.ReceiveNext();
    //    }
    //
    //
    //}

    [PunRPC]
    public void ActualizarGolRojo(int golActualizadoRojo) //Vamos a actualizar variables en cada vez que se haga un gol
    {
        print("GOL ROJITOOOOO" + golActualizadoRojo);
        ui.scoreRojo.text = golActualizadoRojo.ToString();
        golesEquipoRojo = golActualizadoRojo;
    }

    [PunRPC]
    public void ActualizarGolAzul(int golActualizadoAzul) //Vamos a actualizar variables en cada vez que se haga un gol
    {
        print("GOL AZULLL" + golActualizadoAzul);
        ui.scoreAzul.text = golActualizadoAzul.ToString();
        golesEquipoAzul = golActualizadoAzul;
    }

    ////boton exit..
    //public override void OnLeftRoom()
    //{
    //    SceneManager.LoadScene(0);
    //}
    //public void LeaveRoom()
    //{
    //    PhotonNetwork.LeaveRoom();
    //}




}
