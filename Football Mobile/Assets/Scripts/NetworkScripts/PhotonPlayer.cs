﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotonPlayer : MonoBehaviourPunCallbacks
{
    public PhotonView phView;

    //EN ESTE SCRIPT VAN TODAS LAS COSAS QUE TIENEN QUE IR DEL CLIENTE-> AL SERVER Y VISCEVERSA DEL PLAYER

    public int maxPlayersBlue;
    public int maxPlayersRed;
    public int myTeam;
    public GameObject myAvatar;
    bool boolean = true;
    Color color;
    Renderer rend;
    Material mat;

    void Start()
    {      
        if(phView.IsMine) //Si pertenece al jugador local
        {
           
            phView.RPC("RPC_ObtenerEquipos", RpcTarget.MasterClient); //Envia rpc funcion con el cliente master
        }     

    }

    private void Update()
    {
        if (phView.IsMine) //Si pertenece al jugador local
        {

        }
    }

    [PunRPC]
    void RPC_ObtenerEquipos() //Solo se va a ejecutar en el master cliente.
    {
        myTeam = GameInstantiates.gInstantiates.nextPlayerTeam; 
       // GameInstantiates.gInstantiates.UpdateTeam();
        phView.RPC("RPC_EnvioEquipo", RpcTarget.OthersBuffered, myTeam);
    }

    [PunRPC]
    void RPC_EnvioEquipo(int whichTeam)
    {
        print("ENTRE");
        myTeam = whichTeam;
    }

    [PunRPC]
    public void playerTagger(string playerTag)
    {
        gameObject.tag = playerTag;    
    }

    [PunRPC]
    public void playerColor(string colorString) //PlayerColor() recibe colorstring de RPC y hace la 
    {                                           //logica de que color va para cada variable y se la envia a la funcion SetColor().
        if (colorString == "blue")
        {
            color = Color.blue;
        }
        else if (colorString == "red")
        {
            color = Color.red;
        }

        mat = gameObject.GetComponent<Material>();
        rend = gameObject.GetComponent<Renderer>();
        rend.material.shader = Shader.Find("PlayerMaterial");
        rend.material = mat;
        rend.material.SetColor("_Color", color);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom PhotonPlayer!");
    }

}
