﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class GameInstantiates : MonoBehaviourPunCallbacks
{
    public static GameInstantiates gInstantiates;
    public CharacterListManager characterListManager;
    public CharacterList characterScript;
    public Transform[] spawnsRed;
    public Transform[] spawnsBlue;
    public int nextPlayerTeam;
    Renderer rend;
    Material mat;
    
    private Button elegirEquipoAzul;
    private Button elegirEquipoRojo;
    public GameObject cartelEquipoLleno;
    public GameObject uiEquiposPicker;
   
    GameObject Player;
    string playerTag;

    private void Awake()
    {
        if (gInstantiates == null)
        {
            gInstantiates = this;
        }

        characterListManager = FindObjectOfType<CharacterListManager>();
    }

    private void Start()
    {
        characterScript = FindObjectOfType<CharacterList>();     
        uiEquiposPicker = GameObject.FindGameObjectWithTag("TeamPicker");
        cartelEquipoLleno = GameObject.FindGameObjectWithTag("CartelEquipoLleno");
        uiEquiposPicker.SetActive(true);
        cartelEquipoLleno.SetActive(false);
    }

    private void Update()
    {
    }

    //FALTA HACER EL BLOQUEO DE INSTANCIAR CUANDO EL EQUIPO ESTÁ LLENO.

    public void InstanciarEnAzul()
    {
        int spawnPicker = Random.Range(0, GameInstantiates.gInstantiates.spawnsBlue.Length);
        playerTag = "PlayerAzul";
        Vector3 posicionAInstanciar = gInstantiates.spawnsBlue[spawnPicker].position;
        Quaternion rotacion = gInstantiates.spawnsBlue[spawnPicker].rotation;
        string stringColor = "blue";
        InstanciarPlayer(tag, posicionAInstanciar, rotacion, stringColor);          
        uiEquiposPicker.SetActive(false);
    }  
    
    public void InstanciarEnRojo()
    {
        int spawnPicker = Random.Range(0, GameInstantiates.gInstantiates.spawnsRed.Length);
        playerTag = "PlayerRojo";
        Vector3 posicionAInstanciar = gInstantiates.spawnsRed[spawnPicker].position;
        Quaternion rotacion = gInstantiates.spawnsRed[spawnPicker].rotation;
        string stringColor = "red";
        InstanciarPlayer(tag, posicionAInstanciar, rotacion, stringColor);      
        uiEquiposPicker.SetActive(false);
    }

    public void InstanciarPlayer(string tag, Vector3 posicionAInstanciar, Quaternion rotacion, string colorString) //Le mandamos como parametros el color y tag en forma de string. 
    {
        Player = PhotonNetwork.Instantiate(characterListManager.characterActualNombre, posicionAInstanciar, rotacion, 0);
        PhotonView playerPView = Player.GetComponent<PhotonView>();
           
        if (Player != null)
        {
            playerPView.RPC("playerColor", RpcTarget.AllBufferedViaServer, colorString);   //se llaman las funciones RPC QUE ESTÁN EN EL PHOTONPLAYE.   
            playerPView.RPC("playerTagger", RpcTarget.AllBufferedViaServer, playerTag);    //se llaman las funciones RPC QUE ESTÁN EN EL PHOTONPLAYER.                   
        }             
    }


    IEnumerator MostrarCartel()
    {
        cartelEquipoLleno.SetActive(true);
        yield return new WaitForSeconds(1f);
        cartelEquipoLleno.SetActive(false);
        StopCoroutine(MostrarCartel());
    }

}
