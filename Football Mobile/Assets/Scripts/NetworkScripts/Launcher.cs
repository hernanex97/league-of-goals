﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Com.MyCompany.MyGame
{

    public class Launcher : MonoBehaviourPunCallbacks
    {
        #region Private Serializable Fields
        //Acá van cositas privadas que queremos que sean públicas en el Unity xdxd 
        [SerializeField]
        //Maximo de players
        private byte maxPlayersPerRoom = 2;
        #endregion


        #region Private Fields

        string gameVersion = "1";
        bool isConnecting;

        #endregion

        #region Public Fields
        //The Ui Panel to let the user enter name, connect and play
        [SerializeField]
        private GameObject panel;
        //The UI Label to inform the user that the connection is in progress
        [SerializeField]
        private GameObject conectandoTexto;


        #endregion



        public void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public void Start()
        {
            conectandoTexto.SetActive(false);
            panel.SetActive(false);
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }

        public void Conectar1vs1() //Conecta al server de Photon
        {
           
            if (PhotonNetwork.IsConnected)
            {
                 SetearConexion(2, "JuanVSJuan");
            }
        }

        public void Conectar2vs2() 
        {

            if (PhotonNetwork.IsConnected)
            {
                SetearConexion(4, "DosVSDos");
            }
        }

        public void Conectar3vs3()
        {

            if (PhotonNetwork.IsConnected)
            {
                SetearConexion(6, "TresVSTres");
            }
        }

        public void SetearConexion(byte cantPlayersMax,string nombreSala) //Conecta al server de Photon
        {
            isConnecting = true;
            conectandoTexto.SetActive(true);
            panel.SetActive(false);

            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = cantPlayersMax;
            roomOptions.IsVisible = true;
            roomOptions.IsOpen = true;
            PhotonNetwork.ConnectToRegion("sa");
            PhotonNetwork.JoinOrCreateRoom(nombreSala, roomOptions, TypedLobby.Default);
        }

        public override void OnConnectedToMaster()
        {
            conectandoTexto.SetActive(false);
            panel.SetActive(true);

            if (isConnecting)
            { 
                PhotonNetwork.JoinRandomRoom();
                Debug.Log("¡OnConnectedToMaster() ha sido llamado por PUN!");
            }            
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarningFormat("¡ OnDisconnected() ha sido llamado por PUN con razón {0} ", cause);
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.LogWarningFormat("No pudo conectarse al Room. Crearemos uno xd");
            PhotonNetwork.CreateRoom(null, new RoomOptions {MaxPlayers = maxPlayersPerRoom });
            base.OnJoinRandomFailed(returnCode, message);
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("¡Entraste a un Room exitosamente!");

            // #Critical: Lo cargamos solo si somos el primer player. Sinó, confiamos en el `PhotonNetwork.AutomaticallySyncScene` para sincronizar la escena a instanciar.
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log("Selecionando la escena, masterclient");


                // #Critical
                // Load the Room Level.
                PhotonNetwork.LoadLevel("EscenaPartido");
            }
        }





    }

}
